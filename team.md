# EncounterVTT Team
We consider our team to include both core members and valued contributors.

## Core
| Handle | Discord | Forums | Role | Self-summary |
| ------ | ------- | ------ | ---- | ------------ |
| Khionu | Khionu#0047 | Khionu | Project Leader | Quirky, passionate transwoman, looking to make the world better in some way. |
| Disconsented | Disconsented#3405 | Disconsented | Asset Pipeline | A Kiwi. Also, your face. |

# Valued Contributors
While we appreciate all contributions, these members have exemplified what we pride ourselves on in our community.

| Handle | Discord | Forums | Contributions | Self-summary |
| ------ | ------- | ------ | ------------- | ------------ |
